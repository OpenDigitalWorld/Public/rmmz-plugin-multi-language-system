# Multi-Language System Plugin for RPG Maker MZ

This plugin lets you translate game texts to suit your needs. Texts are simply written in JSON files, grouped in language-specific folders. It is also possible to configure fonts specifically for each language. The active language can be selected from the game options window, or at game startup.

## Patches

Use these patches if you need them:
- Patch for Galv MZ plugins.
- Patch for VisuStella MZ plugins.

## Plugin Settings

### General Settings

General settings for language configuration.

- **Option Label** [string]: The label for the language selection option. Can be a translatable text ${<text code>}.

- **Root Folder** [string]: The folder containing all the languages files stored in subdirectories per language.

- **Languages** [array of JSON data]: The list of the languages used in the game.

    - **Language Code** [string]: The code of this language such as the ISO format.

    - **Language Label** [string]: The label of this language in its original translation (displayed as a language selection item).

    - **Language Folder** [string]: The folder containing the JSON files for this language (put inside the <Root Folder>).

    - **Language Font Name** [string]: The font name used as the main font for this language. A value from the list of custom fonts (leave blank for default font name).

    - **Language Font Size** [number]: The font size used for the main font for this language. A number between 0 and 108 (0 uses default font size).

    - **Language Files** [array of string]: The list of the JSON files (without extension) for this language (put inside the <Language Folder>).

    - **Language Core Texts**: Hardcoded game engine texts that cannot be translated from the database.

        - **Miss Label** [string]: The label of the "Miss" text in its original translation.

        - **ON Label** [string]: The "ON" wording of the option value in its original translation.

        - **OFF Label** [string]: The "OFF" wording of the option value in its original translation.

### Selection Scene

Settings for the language selection scene during game statup.

- **Active On Startup** [boolean]: Indicates whether the language selection scene is displayed or not when the game starts.

- **Background** [file]: The image file displayed in the background of the language selection scene.

#### Window

Settings for the language selection scene window.

- **Orientation** [list]: The orientation of the language selection window.

- **Width** [number]: The width of the language selection window. A number in [pixel].

- **Origin** [list]: The origin point for positioning the language selection window.

- **Position X** [number]: The X position the language selection window relative to the screen. A number in [pixel].

- **Position Y** [number]: The Y position the language selection window relative to the screen. A number in [pixel].

- **Background** [list]: The background of the language selection window.

- **Skin** [file]: The skin of the language selection window (leave blank for default window skin).

#### Item

Settings for the language selection scene items.

- **Alignment** [list]: The alignement of the language selection items.

- **Font Name** [string]: The font name of the language selection items. A value from the list of custom fonts (leave blank for default font name).

- **Font Size** [number]: The font size of the language selection items. A number between 0 and 108 (0 uses default font size).

- **Font Style** [list]: The font style of the language selection items.

- **Text Color** [string]: The main color of the language selection items. Can be #HEX, rgba() or a color code from the window skin (0 to 31).

- **Outline Color** [string]: The outline color of the language selection items. Can be #HEX, rgba() or a color code from the window skin (0 to 31).

- **Outline Width** [number]: The outline width of the language selection items. A number between 0 and 12.

## Plugin Commands

_This plugin doesn't use plugin commands._

## Notetags

_This plugin doesn't use notetags._

## Script Calls

To get data on the current active language (read the plugin code for more information):

- **ODW.MLS.currentIndex()** [return number]: Return the current active language index.

- **ODW.MLS.currentCode()** [return string]: Return the language code for the current active language index.

- **ODW.MLS.currentLabel()** [return string]: Return the language label for the current active language index.

- **ODW.MLS.getText(string text)** [return string]: Return the text corresponding to the ${text code} included in the language files for the current active language index.

## Rewriting Core Functions

**New objects:**
- MLS_Scene_Languages
- MLS_Window_Languages

**New properties:**
- ConfigManager.mlsLanguageIndex

**New functions:**
- DataManager.mlsLoadLanguageFile
- DataManager.mlsOnXhrLanguageFileLoad
- DataManager.mlsOnXhrLanguageFileError
- ConfigManager.mlsReadLanguageIndex
- Window_Options.prototype.mlsRefreshLanguage

**Overwrite declarations:**
- Bitmap.prototype.drawText
- ConfigManager.makeData
- ConfigManager.applyData
- TextManager.basic
- TextManager.param
- TextManager.command
- TextManager.message
- Game_System.prototype.mainFontFace
- Game_System.prototype.mainFontSize
- Game_Message.prototype.add
- Game_Message.prototype.setChoices
- Scene_Boot.prototype.start
- Scene_Splash.prototype.gotoTitle
- Window_Base.prototype.textWidth
- Window_Base.prototype.createTextState
- Window_Base.prototype.actorName
- Window_Base.prototype.partyMemberName
- Window_Base.prototype.convertEscapeCharacters
- Window_Options.prototype.addGeneralOptions
- Window_Options.prototype.statusText
- Window_Options.prototype.processOk
- Window_Options.prototype.cursorRight
- Window_Options.prototype.cursorLeft
- Window_NameEdit.prototype.setup

**Destructive declarations:**
- Object.defineProperty(TextManager, 'currencyUnit', {...})
- Scene_Boot.prototype.updateDocumentTitle
- Sprite_Damage.prototype.createMiss
- Window_Options.prototype.booleanStatusText
